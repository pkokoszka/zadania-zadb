import requests
import json
from parser import *
import re
log_file = open('/var/log/apache2/access.log','r')
url = "http://194.29.175.241:5984/p7"
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
format_pat= re.compile(
    r"(?P<host>(?:[\d\.]|[\da-fA-F:])+)\s"
    r"(?P<identity>\S*)\s"
    r"(?P<user>\S*)\s"
    r"\[(?P<time>.*?)\]\s"
    r'"(?P<request>.*?)"\s'
    r"(?P<status>\d+)\s"
    r"(?P<bytes>\S*)\s"
    r'"(?P<referer>.*?)"\s'
    r'"(?P<user_agent>.*?)"\s*'
)
for line in log_file:
	parsed_log = format_pat.match(line).groupdict()
	data = {'host': parsed_log['host'], 'bytes': parsed_log['bytes']}
	r = requests.post(url, data=json.dumps(data), headers=headers)
